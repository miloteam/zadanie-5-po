CXXFLAGS= -Wall -pedantic -std=c++14 -iquote inc

__start__: ./uklad_rownan
	./uklad_rownan r

uklad_rownan: obj/main.o obj/LZespolona.o
	g++ -Wall -pedantic -o uklad_rownan obj/main.o obj/LZespolona.o


obj/main.o: src/main.cpp inc/SWektor.hh inc/LZespolona.hh inc/SMacierz.hh inc/rozmiar.h inc/SUkladRownanLiniowych.hh
	g++ -c ${CXXFLAGS} -o obj/main.o src/main.cpp

obj/LZespolona.o: src/LZespolona.cpp inc/LZespolona.hh
	g++ -c ${CXXFLAGS} -o obj/LZespolona.o src/LZespolona.cpp
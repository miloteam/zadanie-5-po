#include <iostream>

#include "../inc/LZespolona.hh"
#include "../inc/rozmiar.h"

#include <fstream>
#include "../inc/SUkladRownanLiniowych.hh"
using namespace std;



int main(int argc, char *argv[]){
    if (argv[1][0]=='-' && argv[1][1]=='-' && argv[1][2]=='h' && argv[1][3]=='e' && argv[1][4]=='l' && argv[1][5]=='p'){ 
          cout <<"Dozwolone opcje wywolania:" << endl;
          cout << "r: rozwiazanie rownania rzeczywistego" << endl;
          cout << "z: rozwiazanie rownania zespolonego" <<endl;
            return 0;
    }

    switch(argv[1][0]){

        case 'r':{
        UkladRownanL<double,ROZMIAR>  UklRown;
        ifstream plik_wej;
        plik_wej.open("../funkcja_liniowa.txt");
        plik_wej>>UklRown;
        if(cin.fail()){
            cout<<"Blad wczytywania danych z pliku"<<endl;
            cin.clear();
            cin.ignore(1000, '\n');
        } 
        cout<<endl;
        cout<<"Uklad rownan:"<<endl << UklRown <<endl;

        Wektor<double, ROZMIAR> Wynik=UklRown.Rozwiaz();
        cout<<"Wynik obliczen" << endl <<Wynik <<endl;
        
        Wektor<double,ROZMIAR> blad=UklRown.DlugoscWektoraBledu();
        cout<<"Dlugosc wektora bledu obliczen" << endl <<blad<<endl;
 
        cout<<"Koniec programu"<<endl;

        plik_wej.close();
        break;
        }

        case 'z':{
        UkladRownanL<LZespolona,ROZMIAR>  UklRown;
        ifstream plik_wej;
        plik_wej.open("../funkcja_liniowa_ze.txt");
        plik_wej>>UklRown;
        if(cin.fail()){
            cout<<"Blad wczytywania danych z pliku"<<endl;
            cin.clear();
            cin.ignore(1000, '\n');
        } 
        cout<<endl;
        cout<<"Uklad rownan:"<<endl << UklRown <<endl;

        Wektor<LZespolona, ROZMIAR> Wynik=UklRown.Rozwiaz();
        cout<<"Wynik obliczen" << endl <<Wynik <<endl;

        Wektor<LZespolona,ROZMIAR> blad=UklRown.DlugoscWektoraBledu();
        cout<<"Dlugosc wektora bledu obliczen" << endl <<blad<<endl;

        cout<<"Koniec programu"<<endl;

        plik_wej.close();
        break;
        }


        default:{
        cout << "Nieprawidlowe wywolanie programu!" << endl;
        cout << "Wybierz opcje --help by uzyskac pomoc" << endl;
        }
    }
}
#include "../inc/LZespolona.hh"
#include<iostream>
#include<iomanip>
using std::cout;
using std::endl;
using std::cin;


LZespolona::LZespolona(){
  Re = 0;
  Im = 0;
}

LZespolona::LZespolona(double liczba){
  Re = liczba;
  Im = 0;
}

LZespolona &LZespolona::operator=(double liczba){

  Re=liczba;
  Im=0;
  return *this;
}


LZespolona  operator + (LZespolona L1, LZespolona L){
  LZespolona  wynik;

  wynik.Re = L1.Re + L.Re;
  wynik.Im = L1.Im + L.Im;
  return wynik;
}


LZespolona LZespolona::operator +=(const LZespolona &L){
  Re=Re+L.Re;
  Im=Im+L.Im;
  return *this;
}

LZespolona LZespolona::operator *=(const LZespolona &L1){
  Re=Re*L1.Re;
  Im=Im*L1.Im;
  return *this;
}
LZespolona LZespolona::operator - (const LZespolona L) const{
  LZespolona wynik;
  wynik.Re=Re-L.Re;
  wynik.Im=Im-L.Im;
  return wynik;
}
LZespolona LZespolona::operator -(){
  this->Re=-this->Re;
  this->Im=-this->Im;
  return *this;
}
LZespolona LZespolona::operator * (const LZespolona L) const {
  LZespolona wynik;
  wynik.Re=(Re*L.Re)-(Im*L.Im);
  wynik.Im=(Re*L.Im)+(Im*L.Re);
  return wynik;
}
LZespolona LZespolona::operator * (const double liczba )const {
  LZespolona wynik(0);
  wynik.Re=Re*liczba;
  wynik.Im=Im*liczba;
  return wynik;
}
double dzielnik(LZespolona L1, LZespolona L2){
  double a;
  a=L1.Re*L2.Re+L1.Im*L2.Im;
  return a;
}

LZespolona LZespolona::operator / (const LZespolona L)const {
  LZespolona wynik;
  double a;
  a=dzielnik(*this,L);
  wynik.Re=(Re*L.Re+Im*L.Im)/a;
  wynik.Im=(Im*L.Re-Re*L.Im)/a;
  return wynik;
  
}
bool LZespolona::operator ==(LZespolona L){
  return ((Re==L.Re)&&(Im==L.Im));
}

bool LZespolona::operator !=(LZespolona L){
  return !(*this == L);
} 
std::ostream & operator<< (std::ostream &in, const LZespolona &Z1){
  
 in<<"("<<Z1.Re<<std::showpos<<Z1.Im<<std::noshowpos<<"i)";
 return in;
}

std::istream & operator>> (std::istream &out, LZespolona &Z1){
  char znak;
 out>>znak;
  if(znak!='(')
    out.setstate(std::ios::failbit);
 out>>Z1.Re;
 out>>znak;
 out>>Z1.Im;
 if(znak=='-')
    Z1.Im=(-Z1.Im);
 out>>znak;
  if(znak!='i')
    out.setstate(std::ios::failbit);
 out>>znak;
  if(znak!=')')
    out.setstate(std::ios::failbit);
  return out;
}

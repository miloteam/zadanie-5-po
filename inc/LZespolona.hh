#pragma once
#include<iostream>

//Klasa opisującą liczbe zespoloną
struct  LZespolona {
  double   Re;    //Część rzeczywista
  double   Im;    //Część urojona
  
  LZespolona operator +=(const LZespolona &L);
  
  LZespolona();
  LZespolona(double liczba);
  LZespolona &operator=(double liczba);
  LZespolona operator -();
  LZespolona operator *=(const LZespolona &L);

  LZespolona operator / (const LZespolona L)const ;
  LZespolona  operator + (const LZespolona L)const ;
  LZespolona operator - (const LZespolona L) const;
  LZespolona operator * (const LZespolona L) const ;
  LZespolona operator * (const double liczba)const ; 

  bool operator ==(LZespolona L);
  bool operator !=(LZespolona L);

};



double dzielnik(LZespolona L1, LZespolona L2);


std::ostream & operator<<(std::ostream &in, const LZespolona &Z1);
std::istream & operator >>(std::istream &out, LZespolona &Z1);




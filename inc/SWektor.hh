#pragma once

#include <iostream>
#include <string.h>
#include<cmath>

//Klasa opisująca wektor o dowolnym rozmiarze zawierający dowolny typ danych
template<typename Typ, int Rozmiar>
class Wektor {

  Typ tablica [Rozmiar];

  public:

  Wektor();
  
  Wektor<Typ, Rozmiar> operator + (const Wektor<Typ, Rozmiar> & W2) const;
  Wektor<Typ, Rozmiar>  operator - (const Wektor<Typ, Rozmiar>  & W2) const;
 
  Typ operator * (const Wektor<Typ, Rozmiar>  & W2) const;

  Wektor<Typ, Rozmiar>  operator * (double li) const;

 
   
  const Typ & operator[] (unsigned int index) const; 
  Typ & operator[] (unsigned int index);
  
};



/*      Przeciazenie strumienia wejsciowego (wczytywanie wartosci od uzykownika)

 Parametry:
  in - odwołanie do strumienia
  W - adres zmiennej typu wektor, do ktorej wartosci mają być wpisane

 Warunki wstepne:
  in musi być strumieniem wejściowym
 */
 template<typename Typ, int Rozmiar>
 std::istream& operator >> (std::istream &in, Wektor<Typ, Rozmiar>  &W);

/*     Przeciążenie strumienia wyjściowego (wyświetlanie wartości)

 Parametry:
  out - odwołanie do strumienia
  W - wektor, który ma być wyświetlony
 
 Warunki wstępne:
  out musi być strumieniem wyjściowym
   */
template<typename Typ, int Rozmiar>
std::ostream& operator << (std::ostream &out, const  Wektor<Typ, Rozmiar> &W);


template<typename Typ, int Rozmiar>
Wektor<Typ, Rozmiar>  operator * (double l1, Wektor<Typ, Rozmiar> W2);


//-------------------------------------------------------------------------
//Definicje metod:


template<typename Typ, int Rozmiar>
Wektor<Typ,Rozmiar>::Wektor(){
  for(int i=0;i<Rozmiar;i++){
    tablica[i]=0;
  }
}


template<typename Typ, int Rozmiar>
Typ & Wektor<Typ, Rozmiar>::operator[] (unsigned int indeks) { 
    if (indeks < 0 || indeks >= Rozmiar) {
      cerr << "" << endl;
      exit(1);
    }
    return tablica[indeks];     
  }

template<typename Typ, int Rozmiar>
const Typ &Wektor<Typ, Rozmiar>::operator[] (unsigned int indeks) const{
  if (indeks < 0 || indeks >= Rozmiar) {
      cerr << "Blad: wartosc poza zakresem wektora" << endl;
      exit(1);
    }
     return tablica[indeks];
}

template<typename Typ, int Rozmiar>
std::istream &operator>>(std::istream &in, Wektor<Typ, Rozmiar>  &W){
  for(int i=0;i<Rozmiar;i++){
    in>>W[i];
  if(in.fail())
  in.setstate(std::ios::failbit);
  }
  return in;
}

template<typename Typ, int Rozmiar>
std::ostream& operator <<(std::ostream &out, const Wektor<Typ, Rozmiar> &W){
  for(int i=0;i<Rozmiar;i++){
    out<<W[i]<<" ";
  if(out.fail())
    out.setstate(std::ios::failbit);
  } 
  return out;
}

template<typename Typ, int Rozmiar>
Wektor<Typ, Rozmiar> Wektor<Typ, Rozmiar>::operator + (const Wektor<Typ, Rozmiar> & W2) const{
  Wektor<Typ, Rozmiar> wynik;
  for(int i=0;i<Rozmiar;i++){
    wynik.tablica[i]=tablica[i]+W2.tablica[i];
  }
  return wynik;
}

template<typename Typ, int Rozmiar>
Wektor<Typ, Rozmiar> Wektor<Typ, Rozmiar>::operator - (const  Wektor<Typ, Rozmiar>& W2) const{
  Wektor<Typ, Rozmiar> wynik;
  for(int i=0;i<Rozmiar;i++){
    wynik.tablica[i]=tablica[i]-W2.tablica[i];
  }
  return wynik;
}

template<typename Typ, int Rozmiar>
Typ Wektor<Typ, Rozmiar>::operator * (const Wektor<Typ, Rozmiar> & W2) const{
  Typ wynik = 0;
  Typ iloczyn = 0;
  for(int i=0;i<Rozmiar;i++){
    iloczyn=tablica[i]*W2.tablica[i];
    wynik+=iloczyn;
  }
  return wynik;
}

template<typename Typ, int Rozmiar>
Wektor<Typ, Rozmiar> Wektor<Typ, Rozmiar>::operator * (double li) const {
  Wektor<Typ, Rozmiar> wynik;
  for(int i=0;i<Rozmiar;i++){
    wynik.tablica[i]=tablica[i]*li;
  }
  return wynik;
}

template<typename Typ, int Rozmiar>  
Wektor<Typ, Rozmiar> operator * (double l1, Wektor<Typ, Rozmiar> W2){

  for(int i=0;i<Rozmiar;i++){
    W2[i]=W2[i]*l1;
  }
  return W2;
}



#pragma once

#include <iostream>
#include "SMacierz.hh"

using std::endl;
using std::cout;
/*
 * jest to klasa uklad rownan i sklada sie z macierzy oraz wyrazow wolnych.
 * Rozwiazuje dane uklady rownan, w tym zadaniu korzystajac z macierzy o wymiarach podanych w pliku ROZMIAR i wetora o ROZMiAR wyrazach.
 */
template<typename Typ, int Rozmiar>
class UkladRownanL {

  Macierz<Typ,Rozmiar> A;
  Wektor<Typ,Rozmiar> B;

  public:
  UkladRownanL();
  UkladRownanL(Macierz<Typ,Rozmiar> nowe_A, Wektor<Typ,Rozmiar> nowe_B);

  Macierz<Typ,Rozmiar> GetA() const {return A;};
  Wektor<Typ,Rozmiar> GetB() const {return B;}; 

  void SetA(const Macierz<Typ,Rozmiar> nowe_A) {A = nowe_A;};
  void SetB(const Wektor<Typ,Rozmiar> nowe_B) {B = nowe_B;};

  Wektor<Typ,Rozmiar> operator * (const Wektor<Typ,Rozmiar> & W)const;
  Wektor<Typ,Rozmiar> Rozwiaz() const; 
  Wektor<Typ,Rozmiar> DlugoscWektoraBledu()const; //obliczenie dlugosci wektora bledu
};


/*
 * Jest to przeciązenie operatora wczytywania. Powinien wczytywać wartosci potrzebnych do rozwiazania
 * ukladow rownan w formie macierzy rozszerzonej o wyrazy wolne.
 * Wczytuje macierz i wektor w tym wypadku podane w pliku
 */
template<typename Typ, int Rozmiar>
std::istream& operator >> (std::istream &in, UkladRownanL<Typ,Rozmiar> &UklRown);

/*
 * Przeciazenie strumienia wyjsciowego.Funkcja wyswietla dla uzytkownika
*  podana wartosc.
* 
*  Opis poszczegolnych parametrow wywolania funkcji:
*   ostream &strm  - odwolanie do sturmienia poprzez referencje,
*    const UkladRownanL  &UklRown - wartosc,ktora powinna zostac wyswietlona,
*   bez jej zmiany, w dodatku odwolujac sie do oryginalu
*   
*  Warunki wstepne:
*   strm musi byc strumieniem wyjsciowym, by mozna bylo wyswietlic wartosci,
*   watosci musza byc podane w odpowiedniej formie 
*
*  Warunki koncowe:
*   brak
*
*  Funkcja wyswietla na wyjsciu standardowym wartosci ukladu rownan.
 */
template<typename Typ, int Rozmiar>
std::ostream& operator << ( std::ostream &out,const UkladRownanL<Typ,Rozmiar>  &UklRown);

template<typename Typ, int Rozmiar>
UkladRownanL<Typ,Rozmiar>::UkladRownanL(){
    A = Macierz<Typ,Rozmiar>();
    B = Wektor<Typ,Rozmiar>();  
}

template<typename Typ, int Rozmiar>
UkladRownanL<Typ,Rozmiar>::UkladRownanL(Macierz<Typ,Rozmiar> nowe_A, Wektor<Typ,Rozmiar> nowe_B){
    A = nowe_A;
    B = nowe_B;
}

template<typename Typ, int Rozmiar>
std::istream& operator >> (std::istream &in, UkladRownanL<Typ,Rozmiar> &UklRown){
  Macierz<Typ,Rozmiar> A;
  Wektor<Typ,Rozmiar> B;
  in>>A;
  A.Transpozycja();
  UklRown.SetA(A);
  in>>B;
  UklRown.SetB(B);
  if(in.fail())
    in.setstate(std::ios::failbit);
  return in;
}

template<typename Typ, int Rozmiar>
std::ostream& operator << ( std::ostream &out,const UkladRownanL<Typ,Rozmiar>  &UklRown){
  Macierz<Typ,Rozmiar> A=UklRown.GetA();
  Wektor<Typ,Rozmiar> B=UklRown.GetB(); 
    out<<"Macierz poczatkowa"<<endl;
    out<<A; 
    out<<"Wektor wyrazow wolnych"<<endl;
    out<<B;
    if(out.fail())
      out.setstate(std::ios::failbit);
    return out;
} 

template<typename Typ, int Rozmiar>
Wektor<Typ,Rozmiar> UkladRownanL<Typ,Rozmiar>::Rozwiaz() const{
  Wektor<Typ,Rozmiar> wynikW;
  Typ zero;
  zero = 0;
  Typ wynik;
  wynik = 0;
  Typ wyznacznik;
  wyznacznik = A.WyznacznikGauss();

  if(wyznacznik==zero){
    cout<<"Brak rozwiazania"<<endl;
    exit(2);
  }

  for(int i=0;i<Rozmiar;i++){
    Macierz<Typ,Rozmiar> M=A;
    M.ZmienKolumne(B,i); 
    Typ wyznacznik2=M.WyznacznikGauss();
    wynik=wyznacznik2/wyznacznik;
    wynikW[i]=wynik;
  }

  return wynikW;
}

template<typename Typ, int Rozmiar>
Wektor<Typ,Rozmiar> UkladRownanL<Typ,Rozmiar>::DlugoscWektoraBledu()const{
  Wektor<Typ,Rozmiar> W=Rozwiaz();
  Wektor<Typ,Rozmiar> wynik=A*W;
  Wektor<Typ,Rozmiar> blad=wynik-B;
  return blad;
}

#pragma once

#include <iostream>
#include "SWektor.hh"
#include "LZespolona.hh"
#include <string.h>

using std::cerr;
using std::endl;
using std::cout;

//Klasa opisujaca macierz kwadratową o dowolnym rozmiarze zawierającą dowolne typy danych
template<typename Typ, int Rozmiar>

class Macierz{

  Wektor<Typ, Rozmiar> tablicaW [Rozmiar]; // Tablica wektorów, będących wierszami macierzy
  public:

  Macierz();
  
  void Transpozycja(); 
  void Odwroc(); 

  Typ WyznacznikGauss() const; 

  Macierz<Typ, Rozmiar> operator + (const Macierz<Typ, Rozmiar> & M)const;
  Macierz<Typ, Rozmiar> operator - (const Macierz<Typ, Rozmiar> & M)const;
  
  Macierz<Typ, Rozmiar> operator * (const Macierz<Typ,Rozmiar> & M)const;
  Macierz<Typ, Rozmiar>  operator * (double l)const;
  Wektor<Typ, Rozmiar> operator * (const Wektor<Typ,Rozmiar> & W)const;

  const Wektor<Typ, Rozmiar> &operator[] (unsigned int Wiersz) const;
  Wektor<Typ, Rozmiar> &operator[] (unsigned int Wiersz);

  const Typ & operator() (unsigned int Wiersz, unsigned int Kolumna) const;
  Typ & operator() (unsigned int Wiersz,unsigned int Kolumna);

 Wektor<Typ, Rozmiar> ZwrocKolumne(int indeks)const; 
 void ZmienKolumne(Wektor<Typ,Rozmiar> W, int i); 
};

/*      Przeciazenie strumienia wejsciowego (wczytywanie wartosci od uzykownika)

 Parametry:
  in - odwołanie do strumienia
  M - adres zmiennej typu macierz, do ktorej wartosci mają być wpisane

 Warunki wstepne:
  in musi być strumieniem wejściowym
 */
template<typename Typ, int Rozmiar>
std::istream& operator>>(std::istream &in, Macierz<Typ, Rozmiar> &M);


/*     Przeciążenie strumienia wyjściowego (wyświetlanie wartości)

 Parametry:
  out - odwołanie do strumienia
  M - macierz, który ma być wyświetlony
 
 Warunki wstępne:
  out musi być strumieniem wyjściowym
*/
template<typename Typ, int Rozmiar>
std::ostream& operator<<(std::ostream &out, const Macierz<Typ, Rozmiar> &M);




template<typename Typ, int Rozmiar>
Macierz<Typ,Rozmiar>::Macierz(){ 
  for(int i=0;i<Rozmiar;i++){
    for(int j=0;j<Rozmiar;j++){
      tablicaW[i][j]=0; 
    }
  }
}


template<typename Typ, int Rozmiar>
const Wektor<Typ,Rozmiar> &Macierz<Typ, Rozmiar>::operator[] (unsigned int Wiersz) const{
    if (Wiersz < 0 || Wiersz >= Rozmiar) {
      cerr << "Blad: wartosc poza zakresem macierzy" << endl;
      exit(1);
    }
    return tablicaW[Wiersz];
}

template<typename Typ, int Rozmiar>
Wektor<Typ,Rozmiar> & Macierz<Typ, Rozmiar>::operator[] (unsigned int Wiersz){
  if (Wiersz < 0 || Wiersz >= Rozmiar) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  }
  return tablicaW[Wiersz];
}

template<typename Typ, int Rozmiar>
 const Typ & Macierz<Typ,Rozmiar>::operator() (unsigned int Wiersz,unsigned int Kolumna) const{
  if ((Wiersz < 0 || Wiersz >= Rozmiar) && (Kolumna < 0 || Kolumna >= Rozmiar)) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  }
  return tablicaW[Wiersz][Kolumna];
}

template<typename Typ, int Rozmiar>
Typ & Macierz<Typ,Rozmiar>::operator() (unsigned int Wiersz, unsigned int Kolumna){
  if ((Wiersz < 0 || Wiersz >= Rozmiar) && (Kolumna < 0 || Kolumna >= Rozmiar)) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  } 
  return tablicaW[Wiersz][Kolumna];
 }

template<typename Typ, int Rozmiar>
Wektor<Typ,Rozmiar> Macierz<Typ,Rozmiar>::ZwrocKolumne(int ind)const{
  if (ind < 0 || ind >= Rozmiar) {
    cerr << "Blad: wartosc poza zakresem macierzy" << endl;
    exit(1);
  } 
  Wektor<Typ,Rozmiar> Kolumna;
  for(int i=0;i<Rozmiar;i++){
    Kolumna[i]=tablicaW[i][ind];
  }
      
  return Kolumna;
}
 
template<typename Typ, int Rozmiar>
void Macierz<Typ,Rozmiar>::ZmienKolumne(Wektor<Typ,Rozmiar> W, int i){
  for(int j=0;j<Rozmiar;j++){
    tablicaW[j][i]=W[j];
  }
}

template<typename Typ, int Rozmiar>
void Macierz<Typ,Rozmiar>::Transpozycja(){
 Macierz<Typ,Rozmiar> M;
 Wektor<Typ,Rozmiar> W;
 for(int i=0;i<Rozmiar;i++){
   M.tablicaW[i]=tablicaW[i];
 }

 for(int i=0;i<Rozmiar;i++){
   W=M.ZwrocKolumne(i);
   tablicaW[i]=W;
 }
}

template<typename Typ, int Rozmiar>
Typ Macierz<Typ,Rozmiar>::WyznacznikGauss() const{
  
  Macierz<Typ,Rozmiar> M=(*this);
  
  Typ wyznacznik;
  wyznacznik=1;
  Typ iloczyn;
  iloczyn=0;
  for(int i=0;i<Rozmiar-1;i++){
   for(int j=i+1;j<Rozmiar;j++){
    iloczyn=M.tablicaW[j][i]/M.tablicaW[i][i];
    for(int k=i;k<Rozmiar;k++)
    M.tablicaW[j][k]=M.tablicaW[j][k]-iloczyn*M.tablicaW[i][k];
    
   }
 }
 for(int z=0;z<Rozmiar;z++){
   wyznacznik=wyznacznik*M.tablicaW[z][z];
   
 }
 return wyznacznik;
}

template<typename Typ, int Rozmiar>
Macierz<Typ,Rozmiar> Macierz<Typ,Rozmiar>::operator+ (const Macierz<Typ,Rozmiar> & M)const{
  Macierz<Typ,Rozmiar> wynik;
    for(int i=0;i<Rozmiar;i++){
      wynik.tablicaW[i]=tablicaW[i]+M.tablicaW[i];
    }
  return wynik;
}

template<typename Typ, int Rozmiar>
Macierz<Typ,Rozmiar> Macierz<Typ,Rozmiar>::operator - (const Macierz<Typ,Rozmiar> & M)const{
  Macierz<Typ,Rozmiar> wynik;
    for(int i=0;i<Rozmiar;i++){
      wynik.tablicaW[i]=tablicaW[i]-M.tablicaW[i];
  }
  return wynik;
}

template<typename Typ, int Rozmiar>
Macierz<Typ,Rozmiar> Macierz<Typ,Rozmiar>::operator * (const Macierz<Typ,Rozmiar> & M)const{
  Macierz<Typ,Rozmiar> wynik;
  for(int i=0;i<Rozmiar;i++){
    Wektor<Typ,Rozmiar> WK = M.ZwrocKolumne(i);
    for(int j=0;j<Rozmiar;j++){
      wynik.tablicaW[i][j]=WK*tablicaW[i];
    }
  }
  return wynik;
} 

template<typename Typ, int Rozmiar>
Macierz<Typ,Rozmiar> Macierz<Typ,Rozmiar>::operator * (double l)const{
   Macierz<Typ,Rozmiar> M;
   for(int i=0;i<Rozmiar;i++){
    M.tablicaW[i]=tablicaW[i]*l;
   }
   return M;
 }
 
template<typename Typ, int Rozmiar>
Wektor<Typ,Rozmiar> Macierz<Typ,Rozmiar>::operator * (const Wektor<Typ,Rozmiar> &W) const{
  Typ iloczyn;
  Wektor<Typ,Rozmiar> wynik;  
  for(int i=0;i<Rozmiar;i++){
    for(int j=0;j<Rozmiar;j++){
      iloczyn=tablicaW[i][j]*W[j];
      wynik[i]+=iloczyn; 
    }
  }
  return wynik;
}
 

template<typename Typ, int Rozmiar>
std::istream& operator>>(std::istream &in, Macierz<Typ,Rozmiar> &M){
  for(int i=0;i<Rozmiar;i++){
    for(int j=0;j<Rozmiar;j++){
      in>>M(i,j);
      if(in.fail())
      in.setstate(std::ios::failbit);
    }
  } 
  return in;
}

template<typename Typ, int Rozmiar>
std::ostream& operator<<(std::ostream &out, const Macierz<Typ,Rozmiar> &M){
  for(int i=0;i<Rozmiar;i++){
    for(int j=0;j<Rozmiar;j++){
      out<<M(i,j);
      out<<" ";
      if(out.fail()) 
        out.setstate(std::ios::failbit);
    }
    out<<endl;
  }   
  return out;
}
